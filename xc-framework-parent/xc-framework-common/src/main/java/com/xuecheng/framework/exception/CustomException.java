package com.xuecheng.framework.exception;

import com.xuecheng.framework.model.response.ResultCode;

/**
 * @Author: wzh
 * @Date: 2019/8/9 00:49
 * @Description:
 * 自定义异常类
 * 继承RuntimeException运行时异常
 * 项目中用的最多
 * 为什么用呢，对代码的侵入性比较小
 */
public class CustomException extends RuntimeException{

    //错误代码
    ResultCode resultCode;

    //构造方法
    public CustomException(ResultCode resultCode){
        this.resultCode = resultCode;
    }
    //取出错误代码
    public ResultCode getResultCode() {
        return resultCode;
    }
}
