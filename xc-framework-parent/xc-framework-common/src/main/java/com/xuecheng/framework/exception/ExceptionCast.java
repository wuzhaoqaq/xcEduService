package com.xuecheng.framework.exception;

import com.xuecheng.framework.model.response.ResultCode;

/**
 * @Author: wzh
 * @Date: 2019/8/9 00:57
 * @Description:
 * 封装的异常类
 */
public class ExceptionCast {

    //使用此静态方法抛出自定义异常
    public static void cast(ResultCode resultCode){

        throw new CustomException(resultCode);
    }
}
