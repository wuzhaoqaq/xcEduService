package com.xuecheng.api.cms;

import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.request.QueryPageRequest;
import com.xuecheng.framework.domain.cms.response.CmsPageResult;
import com.xuecheng.framework.model.response.QueryResponseResult;
import com.xuecheng.framework.model.response.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @Author: wzh
 * @Date: 2019/7/22 23:23
 * @Description:
 * Api工程的接口将作为各微服务远程调用使用
 *
 * 接口的定义:根据需求分析出参数类型
 *  访问swagger地址
 *  http://localhost:31001/swagger-ui.html
 */
@Api(value="cms页面管理接口",description = "cms页面管理接口，提供页面的增、删、改、查")
public interface CmsPageControllerApi {

    /**
     * 1、分页查询CmsPage 集合下的数据
     * 2、根据站点Id、模板Id、页面别名查询页面信息
     * 3、接口基于Http Get请求，响应Json数据
     */
    /**
     * @param page 当前页码
     * @param size 每页显示记录数
     * @param queryPageRequest 模型类
     * @return
     */
    @ApiOperation("分页查询页面列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name="page",value = "页码",required=true,paramType="path",dataType="int"),
            @ApiImplicitParam(name="size",value = "每页记录数",required=true,paramType="path",dataType="int")
    })
    //分页查询
    public QueryResponseResult findList(int page, int size, QueryPageRequest queryPageRequest) ;

    @ApiOperation("新增页面")
    //新增页面
    public CmsPageResult add(CmsPage cmsPage);

    @ApiOperation("根据页面id查询页面信息")
    //根据页面id查询页面信息
    public CmsPage findById(String id);

    @ApiOperation("修改页面")
    //修改页面
    public CmsPageResult edit(String id ,CmsPage cmsPage);

    @ApiOperation("通过ID删除页面")
    public ResponseResult delete(String id);


}
