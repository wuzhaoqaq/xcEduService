package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @Author: wzh
 * @Date: 2019/7/23 01:34
 * @Description:
 * 同Spring Data JPA一样Spring Data mongodb也提供自定义方法的规则
 */
public interface CmsPageRepository extends MongoRepository<CmsPage,String> {

    /**
     * 根据页面名称查询
     * @param pageName  页面名称
     * @return
     */
    CmsPage findByPageName(String pageName);

    /**
     * 根据页面名称、站点id、页面访问路径查询
     * @param pageName  页面名称
     * @param siteId    站点id
     * @param pageWebPath   页面访问路径
     * @return
     */
    CmsPage findByPageNameAndSiteIdAndPageWebPath(String pageName, String siteId, String pageWebPath);

}
